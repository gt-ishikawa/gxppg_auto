# GxPPG自動ログインツール

GxPPGに自動ログインするためのためのツール。

１．login.txtのURL・プロセス名とユーザーID・パスワードを自分の情報に置き換える
２．GXPPGAuto.exeを起動

```
【GxPHD】
https://pg.gxp.jp/gxp/app/app.publish/pg_gxp.application
pg_gxp
【GRS】
https://pg.gxp.jp/grs/app/app.publish/pg_grs.application
pg_grs
【GRI】
https://pg.gxp.jp/gri/app/app.publish/pg_gri.application
pg_gri
【SSE】
https://pg.gxp.jp/sse/app/app.publish/pg_sse.application
pg_sse
【GxP】（2018/11/07～）
https://pg.gxp.jp/gxpbs/app/app.publish/pg_gxpbs.application
pg_gxpbs
【Graat（2018/11/07～）
https://pg.gxp.jp/graat/app/app.publish/pg_graat.application
pg_graat
```
