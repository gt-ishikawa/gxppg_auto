﻿using System;
using Codeer.Friendly.Dynamic;
using Codeer.Friendly.Windows;
using Codeer.Friendly.Windows.Grasp;
using RM.Friendly.WPFStandardControls;
using System.Diagnostics;
using System.Windows;
using System.Threading;

namespace GXPPGAuto
{
    public static class Program
    {
        private static WindowsAppFriend _app;

        public static void Main(string[] args)
        {
            AutoLogin();
        }

        private static void AutoLogin()
        {
            // 1行目がURL、2行目がユーザーID、3行目がパスワード、4行目がプロセス名
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + @"login.txt");

            // ******************* //
            //    アプリ起動処理   //
            // ******************* //
            // GxPPG起動
            Process.Start(new ProcessStartInfo()
            {
                FileName = "msedge.exe",
                Arguments = @lines[0],
                CreateNoWindow = true,
                RedirectStandardOutput = false,
                WindowStyle = ProcessWindowStyle.Minimized,
                UseShellExecute = true,
            });

            // プロセス取得
            System.Diagnostics.Process[] ps = null;
            while (ps == null || ps.Length == 0)
            {
                // ※Windows11だといまいち安定しないのでSleepを入れる
                Thread.Sleep(1000);
                ps = System.Diagnostics.Process.GetProcessesByName(lines[3]);
            }
            // ※Windows11だといまいち安定しないのでSleepを入れる
            Thread.Sleep(500);
            var process = Process.GetProcessById(ps[0].Id);
            _app = new WindowsAppFriend(process);

            // MainWindowを取得
            // ※Windows11だといまいち安定しないのでSleepを入れる
            Thread.Sleep(500);
            dynamic main = _app.Type<Application>().Current.MainWindow;
            var activeWindow = new WindowControl(main);

            // 画面要素取得
            // ユーザーID
            WPFTextBox loginID = new WPFTextBox(activeWindow.IdentifyFromLogicalTreeIndex(0, 0, 0, 0, 6, 15));
            // パスワード
            dynamic password = activeWindow.IdentifyFromLogicalTreeIndex(0, 0, 0, 0, 6, 17).Dynamic();
            // ログインボタン
            WPFButtonBase loginButton = new WPFButtonBase(activeWindow.IdentifyFromLogicalTreeIndex(0, 0, 0, 0, 6, 19, 0));

            // ******************* //
            //   自動ログイン実行  //
            // ******************* //
            // ユーザーID入力
            loginID.EmulateChangeText(lines[1]);
            // パスワード入力
            password.Password = lines[2];
            // 早すぎるので待ってみる
            Thread.Sleep(1500);
            // ログイン実行
            loginButton.EmulateClick();
        }
    }
}
